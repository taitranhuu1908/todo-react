import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class BoardDto {
  @IsNotEmpty()
  @IsString()
  name: string;
  @IsString()
  @IsOptional()
  description?: string;
  @IsOptional()
  avatar?: string;
  @IsOptional()
  @IsNumber()
  backgroundId: number;
}

export const SelectBoardDto = {
  owner: true,
  background: true,
  members: {
    select: {
      user: {
        select: {
          id: true,
          email: true,
          profile: {
            select: {
              avatar: true,
              fullName: true,
            },
          },
        },
      },
    },
  },
};
