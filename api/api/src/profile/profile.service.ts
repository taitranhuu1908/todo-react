import { ImageService } from './../helper/image.service';
import { ProfileDto } from './../dto/profile.dto';
import { PrismaService } from './../prisma/prisma.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class ProfileService {
  constructor(
    private prisma: PrismaService,
    private imageService: ImageService,
  ) {}

  async updateProfile(userId: number, profileData: ProfileDto) {
    const { avatar } = profileData;
    const { src } = await this.imageService.writeBase64(avatar);

    if (src) {
      profileData.avatar = src;
    } else {
      delete profileData.avatar;
    }

    const profile = this.prisma.profile.upsert({
      where: {
        userId,
      },
      update: {
        ...profileData,
      },
      create: {
        userId,
        ...profileData,
      },
    });
    return profile;
  }
}
