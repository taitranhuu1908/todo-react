import { BoardDto, SelectBoardDto } from './../dto/board.dto';
import { PrismaService } from './../prisma/prisma.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class BoardService {
  constructor(private prisma: PrismaService) {}

  async getBoards(userId: number) {
    const boards = await this.prisma.board.findMany({
      where: {
        ownerId: userId,
        members: {
          every: {
            userId: userId,
          },
        },
      },
      include: SelectBoardDto,
    });

    return boards;
  }

  async createBoard(userId: number, boardDto: BoardDto) {
    const background = await this.prisma.background.findUnique({
      where: {
        id: boardDto.backgroundId,
      },
    });

    const board = await this.prisma.board.create({
      data: {
        ownerId: userId,
        ...boardDto,
        backgroundId: background ? background.id : null,
      },
      include: {
        background: true,
        owner: true,
      },
    });

    await this.prisma.member.create({
      data: {
        boardId: board.id,
        userId: userId,
      },
    });

    return board;
  }
}
