export const backgrounds = [
  {
    src: 'https://trello-backgrounds.s3.amazonaws.com/SharedBackground/2106x1600/f87cba890229f45ba094b944f2286467/photo-1654737178110-977669b9bcdc.jpg',
  },
  {
    src: 'https://trello-backgrounds.s3.amazonaws.com/SharedBackground/2048x1152/fb4606e67b893e8004d6c9fc924d2764/photo-1654554396063-09b7b57e7c74.jpg',
  },
  {
    src: 'https://trello-backgrounds.s3.amazonaws.com/SharedBackground/2048x1152/9587db78b11d1d79ac5a408d7fdeb685/photo-1623333769926-a34d46b5fbdb.jpg',
  },
  {
    src: 'https://trello-backgrounds.s3.amazonaws.com/SharedBackground/2048x1190/320fbc510bb233a2a077bc1dba6e5a73/photo-1654204933947-f3d067d7151e.jpg',
  },
];
