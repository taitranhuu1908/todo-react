import React, { useEffect } from 'react';
import LayoutComponent from "./components/layout/Layout.component";


function Hello() {
	return <h1>123</h1>
}

function App() {
	return (
		<>
			<LayoutComponent children={ <Hello/> }/>
		</>
	)
}

export default App;
