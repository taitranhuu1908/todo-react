import { Body, Controller, Put, UseGuards } from '@nestjs/common';
import { GetUser } from 'src/auth/decorator';
import { JwtGuard } from './../auth/guard/jwt.guard';
import { ProfileService } from './profile.service';

@Controller('profile')
@UseGuards(JwtGuard)
export class ProfileController {
  constructor(private profileService: ProfileService) {}

  @Put('/')
  updateProfile(@GetUser('id') userId: number, @Body() profileData) {
    return this.profileService.updateProfile(userId, profileData);
  }
}
