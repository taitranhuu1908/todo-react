import { GENDER } from '@prisma/client';
import { IsBase64, IsString } from 'class-validator';

export class ProfileDto {
  @IsString()
  fullName?: string;
  @IsBase64()
  avatar?: string;
  @IsString()
  birthday?: string;
  @IsString()
  address?: string;
  @IsString()
  phone?: string;
  @IsString()
  gender?: GENDER;
}
