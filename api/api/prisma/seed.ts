import { backgrounds } from './background';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

async function main() {
  await prisma.background.deleteMany({});
  for (const item of backgrounds) {
    await prisma.background.create({
      data: {
        src: item.src,
      },
    });
  }
}

main()
  .catch((e) => {
    console.log(e);
    process.exit(1);
  })
  .finally(() => {
    prisma.$disconnect;
  });
