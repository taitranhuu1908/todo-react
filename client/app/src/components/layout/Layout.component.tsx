import React, { Component } from 'react';
import PropTypes from "prop-types";
import './layout.component.scss'
import HeaderComponent from "../header/header.component";
import FooterComponent from "../footer/footer.component";

const Props = {
	children: PropTypes.node.isRequired,
}

const LayoutComponent = ( { children }: PropTypes.InferProps<typeof Props> ) => {
	return (
		<div>
			<HeaderComponent/>
			{ children }
			<FooterComponent/>
		</div>
	);
};

LayoutComponent.propTypes = Props;

export default LayoutComponent;
