import {
  Body,
  Controller,
  Get,
  HttpCode,
  UseGuards,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { GetUser } from 'src/auth/decorator';
import { JwtGuard } from './../auth/guard/jwt.guard';
import { BoardDto } from './../dto/board.dto';
import { BoardService } from './board.service';

@Controller('board')
@UseGuards(JwtGuard)
export class BoardController {
  constructor(private boardService: BoardService) {}

  @HttpCode(HttpStatus.OK)
  @Get('/')
  getBoards(@GetUser('id') userId: number) {
    return this.boardService.getBoards(userId);
  }

  @Post('/')
  createBoard(@GetUser('id') userId: number, @Body() boardDto: BoardDto) {
    return this.boardService.createBoard(userId, boardDto);
  }
}
