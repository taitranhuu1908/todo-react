import { Injectable } from '@nestjs/common';
import * as fs from 'fs';

@Injectable()
export class ImageService {
  async splitBase64(base64: string) {
    const isBase64 = /^data:image\/png;base64,/.test(base64);
    if (isBase64) {
      const base64Data = base64.replace(/^data:image\/png;base64,/, '');
      const type = base64.split(';')[0].split('/')[1];
      const generateName = `${Date.now()}.${type}`;

      return {
        data: base64Data,
        name: generateName,
      };
    }

    return false;
  }

  async checkFolder(dir: string) {
    if (!fs.existsSync('public')) {
      fs.mkdirSync('public');
    }

    if (!fs.existsSync(`public/${dir}`)) {
      fs.mkdirSync(`public/${dir}`);
    }
  }

  async writeBase64(base64: string) {
    const isBase64 = await this.splitBase64(base64);
    const dir = 'uploads';
    let result = {
      src: null as string | null,
    };

    if (!isBase64) return result;

    const { data, name } = isBase64;

    this.checkFolder(dir);

    fs.writeFile(
      `public/${dir}/${name}`,
      data,
      { encoding: 'base64' },
      function (err) {
        if (err) {
          console.log(err);
        } else {
          console.log('File created');
        }
      },
    );

    const imageLocation = `${process.env.APP_URL}/${dir}/${name}`;

    result = {
      ...result,
      src: imageLocation,
    };

    return result;
  }
}
